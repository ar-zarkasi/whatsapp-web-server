const nasihat = [
    'Penuhi hatimu dengan Iman, niscaya hatimu itu akan menjadi tempat yang paling damai di muka bumi',
    'Jarak terdekat antara masalah dan solusinya adalah sejauh jarak antara lutut dengan lantai (shalat)',
    'Orang yang selalu bersyukur mengucap Alhamdulillah tidak punya waktu untuk mengeluh.',
    '"Sungguh, siapa pun (dengan sengaja) membunuh dirinya sendiri, maka pasti ia akan disiksa di neraka, di mana ia akan tinggal selamanya."\n\n-HR Bukhari Muslim',
    '"Jangan iri kecuali kepada dua perkara: (1) orang yang dianugerahi Allah harta kekayaan kemudian ia membelanjakannya di jalan yang benar, dan (2) orang yang diberi hikmah oleh Allah (pengetahuan tentang alquran dan hadis) kemudian ia melaksanakan dan mengajarkannya."\n\n-HR. Bukhari',
    'Segala hal buruk yang terjadi dalam hidup akan membuka mata terhadap hal-hal baik yang kurang begitu kamu perhatikan sebelumnya',
    'Iman adalah percaya kepada Tuhan, bahkan ketika kamu tidak bisa memahami rencana-Nya',
    '"Ketika hati mengeras, mata akan mengering (sulit menangis)."\n\n\–Ibn Qayyim',
    'Tetapi kalian lebih memilih kehidupan dunia. Padahal, kehidupan akhirat lebih baik dan lebih kekal.\n\n(Q.S Al-A’la: 16-17)',
    'Bersabarlah karena takdir yang tertulis untukmu ditulis oleh penulis terhebat, yaitu Allah SWT',
    'Teruslah dekat dengan sesuatu yang mengingatkanmu kepada Allah',
];
module.exports = nasihat;