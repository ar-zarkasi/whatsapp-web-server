require('dotenv').config()
const { Client, MessageMedia, ClientInfo } = require('whatsapp-web.js');
const express = require('express');
const socketIO = require('socket.io');
const qrcode = require('qrcode');
const http = require('http');
const fs = require('fs');
const { phoneNumberFormatter } = require('./formatter');
const { postServer, getServer } = require('./sending');
let session_chat = [];
// const randomkocak = require("./kocak");
// const randomnasihat = require("./nasihat");
const { type } = require('express/lib/response');
const app = express();
const port = process.env.PORT;
const serverHost = process.env.API_URL;

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

const server = http.createServer(app);
const io = socketIO(server, {
    cors: {
        origin: "*"
    }
});

const logoutData = async (client, socket, data, agree = true) => {
    if (agree) {
        try {
            const resp = await postServer(serverHost + '/whatsapp/disconnected', { id: data.id, message: data.reason });
            if (resp.data.status !== 200) {
                client.destroy();
                client.initialize();
                socket.emit('message', { text: 'Whatsapp is disconnected!' });
            }
            socket.emit('message', { text: resp.data.message });
            socket.emit('disconnect-wa', data)
        } catch (error) {
            console.error(error);
        }   
    } else {
        socket.emit('disconnect-wa', data)
    }
}

app.get('/', (req, res) => {
    res.send({
        status: true,
        message: "Siap Digunakan"
    });
});

const sessions_wa = [];
/* const SESSIONS_FILE = './wa-session.json';

const createSessionsFileIfNotExists = function () {
    if (!fs.existsSync(SESSIONS_FILE)) {
        try {
            fs.writeFileSync(SESSIONS_FILE, JSON.stringify([]));
            console.log('Sessions file created successfully.');
        } catch (err) {
            console.log('Failed to create sessions file: ', err);
        }
    }
}

createSessionsFileIfNotExists(); */

/* const setSessionsFile = async function (sessions) {
    try {
        const senddata = { message: "SUCCESS", data: sessions };
        const resp = await postServer(serverHost + '/whatsapp/saving-session', senddata);
        if (resp.status !== 200) throw resp.message;
    } catch (error) {
        console.log(error);
    }
    fs.writeFile(SESSIONS_FILE, JSON.stringify(sessions), function (err) {
        if (err) {
            console.log(err);
        }
    });
} */

/* const getSessionsFile = async function (id) {
    try {
        const resp = await getServer(serverHost + '/whatsapp/' + id);
        if (resp.status !== 200) throw resp.message;
        return resp.data;
    } catch (error) {
        console.log(error);
    }
    // return JSON.parse(fs.readFileSync(SESSIONS_FILE));
} */

const createSession = async function (socketio, database) {
    let sessionCfg;
    socketio.emit('message', { text: database.text });
    try {
        let ceksession = await getServer(serverHost + '/whatsapp/' + database.id);
        console.log(ceksession.data);
        if (ceksession.data.status === 200) {
            if(ceksession.data.data !== null) sessionCfg = JSON.parse(ceksession.data.data);
        }
        else throw ceksession.data.message;
    } catch (error) {
        socketio.emit('message', { text: error });
        console.log(error);
        return false; // stopping if error API
    }
    // const SESSION_FILE_PATH = `./wa/wa-session-${id}.json`;
    /* if (fs.existsSync(SESSION_FILE_PATH)) {
        sessionCfg = require(SESSION_FILE_PATH);
    } */
    let client = sessions_wa.find(sess => sess.id == database.id);
    if (typeof client === 'undefined'){
        client = new Client({
            restartOnAuthFail: true,
            puppeteer: {
                headless: true,
                args: [
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--disable-dev-shm-usage',
                    '--disable-accelerated-2d-canvas',
                    '--no-first-run',
                    '--no-zygote',
                    '--single-process', // <- this one doesn't works in Windows
                    '--disable-gpu'
                ],
            },
            qrRefreshIntervalMs: 25000,
            session: sessionCfg
        });

        client.initialize();

        client.on('qr', (qr) => {
            try {
                console.log('Send QR Code ', database.id);
                qrcode.toDataURL(qr, (err, url) => {
                    socketio.emit('qr', { id: database.id, src: url });
                    socketio.emit('message', { text: 'QR Code received, scan please!' });
                });
            } catch (error) {
                console.log('Error Send QR timeout');
            }
            
        });

        client.on('ready', async () => {
            console.log('Whatsapp Web Ready to Use');
            try {
                const inf = client.info;
                const infodata = {
                    id: database.id,
                    status: 'ready', message: 'Whatsapp Ready',
                    name: inf.pushname,
                    platform: inf.platform + ' ' + inf.phone.os_version,
                    number: inf.me.user,
                    wa_version: inf.phone.wa_version,
                    device: inf.phone.device_manufacturer
                };
                const resp = await postServer(serverHost + '/whatsapp/status-update', infodata);
                if (resp.data.status === 200) {
                    socketio.emit('ready', infodata);
                    // socketio.emit('ready', { id: database.id });
                    socketio.emit('message', { text: 'Whatsapp is ready!' });
                } else {
                    throw resp.data.message;
                }
            } catch (error) {
                console.log(error);
                socketio.emit('message', { text: error });
            }


            /* const savedSessions = getSessionsFile();
            const sessionIndex = savedSessions.findIndex(sess => sess.id == id);
            savedSessions[sessionIndex].ready = true;
            savedSessions[sessionIndex].clientID = [];
            savedSessions[sessionIndex].path = SESSION_FILE_PATH;
            setSessionsFile(savedSessions); */
        });

        client.on('authenticated', async (session) => {
            console.log('Authenticated');
            try {
                const resp = await postServer(serverHost + '/whatsapp/save-session/' + database.id, { sesi: JSON.stringify(session) });
                if (resp.data.status === 200) {
                    sessionCfg = session;
                    socketio.emit('message', { text: 'Whatsapp is authenticated!' });
                    socketio.emit('authenticated', { id: database.id });
                } else throw resp.data.message;
            } catch (error) {
                console.log(error);
                socketio.emit('message', { text: error });
            }
            /* fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
                if (err) {
                    socketio.emit('message',err);
                }
            }); */
        });

        client.on('auth_failure', function (session) {
            socketio.emit('message', { text: 'Auth failure, restarting...' });
            socketio.emit('disconnect-wa', database.id);
        });

        client.on('message', async (msg) => {
            try {
                const chat = await msg.getChat();
                const contact = await msg.getContact();
                // const session_name = msg.deviceType + '#' + msg.from + '#' + msg.type;
                if (typeof chat !== 'undefined' && chat.isGroup === false) {
                    // cek nama pengirim
                    let nama = null;
                    if (typeof contact.pushname !== 'undefined') nama = contact.pushname;
                    else if (typeof contact.verifiedName !== 'undefined') nama = contact.verifiedName;
                    else nama = contact.name;

                    // cek tipe pesan
                    let pesan = null;
                    let tipe = msg.type;;
                    if (msg.type === 'chat' || msg.type === 'text') {
                        pesan = msg.body;
                        tipe = 'text';
                    } else if (msg.type === 'location') {
                        pesan = {
                            image: msg.body,
                            location: msg.location,
                        }
                    } else if (msg.type === 'image' && msg.hasMedia) {
                        const media = await msg.downloadMedia();
                        pesan = {
                            src: media.data,
                            filename: media.filename,
                            mime: media.mimetype,
                        }
                    }

                    const datasend = {
                        fromclient: database.id,
                        id: msg.id.id,
                        timestamp: chat.timestamp,
                        from: contact.number,
                        text: pesan,
                        type: tipe,
                        businessAccount: contact.isBusiness,
                        enterpriseAccount: contact.isEnterprise,
                        sender: nama,
                        device: msg.deviceType,
                        fromContact: contact.isMyContact,
                        broadcast: msg.broadcast,
                        isForward: msg.isForwarded
                    };
                    const resp = await postServer(serverHost + '/whatsapp/delivery-chat', datasend);
                    console.log(resp.data);
                }    
            } catch (error) {
                console.log('Sleep Mode Browser');    
            }
            
            /* if (!chat.isGroup) {
                let date = new Date();
                let active_chat = session_chat.find(sess => sess.name == session_name);
                let sess_index = session_chat.indexOf(sess => sess.name == session_name);
                if (typeof active_chat === 'undefined') {
                    date.setTime(date.getTime() + (60 * 3000)); // +1 minute expire
                    active_chat = {
                        name: session_name,
                        level: 'start',
                        expired: date,
                    };
                    session_chat.push(active_chat);
                    console.log('masuk chat baru');
                } else {
                    if (active_chat.level === 'wait' && active_chat.expired < date) {
                        active_chat.level = "start"; // fresh mulai baru
                    } else if (active_chat.level !== 'wait') {
                        date.setTime(date.getTime() + (60 * 3000)); // +1 minute expire
                        active_chat.expired = date;
                    }
                }
                if (active_chat.level == "start") {
                    msg.reply('Halo ' + chat.name + ' ,  Si pemilik sedang offline, ada yang bisa saya bantu ?\nAtau sambil menunggu anda dapat memilih konten berikut :\n\n\n\n\n' + greetingOption() + '\n\n\n\nContoh : *1*');
                    active_chat.level = "content"
                } else if (active_chat.level !== "wait") {
                    if (message == '1') {
                        const randomni = Math.floor(Math.random() * randomkocak.length);
                        msg.reply(randomkocak[randomni]);
                    } else if (message == '2') {
                        const randomni = Math.floor(Math.random() * randomnasihat.length);
                        msg.reply(randomnasihat[randomni]);
                    } else if (message == '3') {
                        msg.reply('Jangan Lupa Follow ya 🙏\n\n\n\n\n\n👉https://www.instagram.com/m_a_zarkasi👈');
                    } else if (message == '4') {
                        msg.reply('Mohon Menunggu ..... 🙏');
                        active_chat.level = "wait";
                    } else if (message == '0') {
                        active_chat.level = "end";
                    } else {
                        msg.reply("Maaf " + chat.name + ", format pilihan anda salah, silahkan ketik nomor pilihan anda ?\n\n\n\n" + greetingOption())
                    }

                    if (active_chat.level === "end") {
                        session_chat.splice(sess_index); // remove index
                    }
                } else if (active_chat.level === "wait") {
                    if (message == "end" || message == "0")
                        session_chat.splice(sess_index); // remove index
                }
                console.log("Chat Dari " + chat.name + ":" + msg.body);
                console.log(session_chat);
            } */

        });

        client.on('disconnected', async (reason) => {
            await logoutData(client, socketio, { id: database.id, reason })
            
            /* fs.unlinkSync(SESSION_FILE_PATH, function (err) {
                if (err) return console.log(err);
                console.log('Session file deleted!');
            }); */

            // Menghapus pada file sessions
            /* const savedSessions = getSessionsFile();
            const sessionIndex = savedSessions.findIndex(sess => sess.id == id);
            savedSessions.splice(sessionIndex, 1);
            setSessionsFile(savedSessions);

            io.emit('remove-session', id); */
        });

        // Tambahkan client ke sessions
        sessions_wa.push({
            id: database.id,
            client: client,
            socket: socketio
        });
    } else { // if client found and connected back to UI
        client = client.client;
        socketio.emit('message',{ text: "Get Data..."})
        const info = client.info;
        if (typeof info !== 'undefined') {
            const infodata = {
                id: database.id,
                status: 'ready', message: 'Whatsapp Ready',
                name: info.pushname,
                platform: info.platform + ' ' + info.phone.os_version,
                number: info.me.user,
                wa_version: info.phone.wa_version,
                device: info.phone.device_manufacturer
            };
            socketio.emit('message', { text: 'Whatsapp is ready!' });
            socketio.emit('ready', infodata);   
        }
    }
}

const init = async function (socket) {
    if (socket) {
        socket.emit('init', "Ready");
    } else {
        const savedSessions = await getServer(serverHost + '/whatsapp/all');
        if(savedSessions.data.status === 200)
            await savedSessions.data.data.forEach(sess => {
                console.log("SESSION "+sess.id);
                sess.text = "Reboot All Session";
                const sock = assemble(sess.id)
                // createSession(sock, sess);
            });
    }
}

init();

// Socket IO
const assemble = function (id) {
    const sck = io.of('/' + id)
    createSession(sck, {id, text: 'Restoring Session'});
    sck.on('connection', (socket) => {
        console.log('Socket For '+id+' Ready');
        socket.emit('message', { text: 'Server Ready' });
        socket.on('create-session', (data) => {
            socket.emit('message', { text: 'Getting Session' });
            createSession(socket, data);
        })
        socket.on('log-out-wa', async (data) => {
            console.log("Logged Out ID", data.id);
            const sesi = sessions_wa.find(sess => sess.id == data.id);
            if (typeof sesi !== 'undefined') {
                const index = sessions_wa.indexOf(sess => sess.id == data.id);
                const client = sesi.client
                await client.logout();
                logoutData(client, socket, { id: data.id, reason: 'Logged Out Client Activity' }, false);
                sessions_wa.splice(index, 1);
            }
    
        })
    });
    return sck;
}
io.on('connection', function (socket) {
    console.log('Socket Server Ready');
    socket.on('create-user-socket', function (data) {
        console.log('Membuat user: ', data);
        const sock = assemble(data.id);
        socket.emit('success-creating', data);
    });
    socket.on('check-client-connected', (data) => {
        socket.emit('answer-check-client', { jumlah: sessions_wa.length });
    })
});

function greetingOption() {
    return "*1*. Konten Pantun Garing\n*2*. Nasihat Religi\n*3*. Instagram dari saya\n*4*. Tunggu Chat dari saya\n*0*. Leave Chat";
}

// Send message
app.post('/send-message', (req, res) => {
    const sender = req.body.sender;
    const number = phoneNumberFormatter(req.body.number);
    const message = req.body.message;
    const type = req.body.type;

    const client = sessions_wa.find(sess => sess.id == sender).client;

    client.sendMessage(number, message).then(response => {
        res.status(200).json({
            status: true,
            data: response
        });
    }).catch(err => {
        res.status(500).json({
            status: false,
            data: err
        });
    });
});

server.listen(port, function () {
    console.log('App running on *: ' + port);
});