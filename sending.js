const axios = require('axios');
const https = require('https');
const agent = new https.Agent({  
  rejectUnauthorized: false
});
const options = { httpsAgent: agent };
const postServer = (url,data) => {
    return axios.post(url, data, options);
}
const getServer = (url) => {
    return axios.get(url, options);
}
module.exports = {
    postServer,
    getServer
}